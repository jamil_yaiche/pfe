import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(private router:Router) { }
  title = 'Frontend2';
  ngOnInit() {
    if (localStorage.getItem('islogged')!='true' && window.location.pathname!="/login"){
      this.router.navigate(['/login'])

      
    }
  }
}
