import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  @Input() public router:Array<any> =[];
  user: User;

  constructor(private redirect:Router) { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('user'))
  }

  logout(){
    localStorage.setItem('islogged','false')
    this.redirect.navigate(['/login']);

  }

}
