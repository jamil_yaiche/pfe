import { User } from './user'

export class Project {

  constructor(
    public id?: string,
    public nom?: string,
    public startDate?: Date,
    public endDate?: Date,
    public details?: string,
    public budget?: number,
    public manager = new User(),
    public status?: string,
    public creator?: User,
    public createDate?: Date
  ) {
  };
}