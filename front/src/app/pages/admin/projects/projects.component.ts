import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/Porject';
import { ProjectService } from 'src/app/services/project.service';
import swal from 'sweetalert2';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { UserServiceService } from 'src/app/services/user-service.service';
import { User } from 'src/app/models/user';
declare var $: any;

//import 'src/assets/js/scripts/pickers/dateTime/pick-a-datetime.js';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  public rangeDate:any;
  public inirangeDate:any;

  public project=new Project;
  public managers: Array<User> = [];

  router={
    name:"Projects",
    parent:[
      { name: "home", url: "/home" }
    ]
  }
  projects: Array<Project>;
  
  constructor(private projectsSer: ProjectService,
    private usersSer: UserServiceService) { }

 ngOnInit() {
    this.getAllProjects();
    this.getAllUsers();
    $('button').click(function(){
       });
  }
  getAllUsers() {
    this.usersSer.getAllUsers().subscribe(
      resp => {
        this.managers = resp
      }
      , err => { console.log(err) }
    )
  }

  openAddModal(form){
    this.project= new Project();
    this.rangeDate=[]
    form.clicked=false
  }
  openEditProject(form,project){
    this.project=Object.assign({}, project);
    this.rangeDate=[new Date(this.project.startDate),new Date(this.project.endDate)]
    form.clicked=false;
  }

  removeProject(login) {
    swal.fire(
      {
        icon: 'warning',
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        showCancelButton: true,
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-danger'
        },
      }).then((result) => {
        if (result.value) {
          this.projectsSer.removeProject(login).subscribe(

            resp => {
              console.log(resp);
              swal.fire(
                'Project Deleted!',
                'You have Deleted this project',
                'success'
              )
              this.getAllProjects();


            },
            err => {
              console.log(err)
              swal.fire(
                'Something Bad Happend!',
                'Please try again',
                'error'
              )
            }
          )
        }
      })

  }
  
  addProject(Form) {
    Form.clicked=true
    if (Form.valid) { 
      this.project.startDate=this.rangeDate[0]
      this.project.endDate=this.rangeDate[1]
      this.project.createDate=new Date();
      this.project.creator=JSON.parse(localStorage.getItem('user'))
      this.projectsSer.addProject(this.project).subscribe(
        resp => {
          console.log(resp);
          swal.fire(
            
            resp['response'].toString(),
            '',
            'success'
          )
          document.getElementById('closeModal').click()
          this.getAllProjects();
        },
        err => {
          console.log("add project",err.error)
          swal.fire(
            'Something Bad Happend!',
            err.error,
            'error'
          )
        }

      )
    }
    
  }
  getAllProjects() {
    this.projectsSer.getAllProjects().subscribe(
      resp => {
        this.projects = resp
      }
      , err => { console.log(err) }
    )
  }
}
