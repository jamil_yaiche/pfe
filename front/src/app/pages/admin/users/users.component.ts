import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { UserServiceService } from 'src/app/services/user-service.service';
import { User } from 'src/app/models/user';
import swal from 'sweetalert2';
import { format } from 'url';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {


  public user: User = new User();

  router = {
    name: "Users",
    parent: [
      { name: "home", url: "/home" }
    ]
  }
  users: Array<User>;
  constructor(private usersSer: UserServiceService) { }

  ngOnInit() {
    this.getAllUsers();


  }

  openAddUser(form) {
    this.user = new User();
    form.clicked = false
  }
  openEditUser(form, user) {
    this.user = Object.assign({}, user);
    form.clicked = false;
  }

  removeUser(login) {
    swal.fire(
      {
        icon: 'warning',
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        showCancelButton: true,
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-danger'
        },
      }).then((result) => {
        if (result.value) {
          this.usersSer.removeUser(login).subscribe(

            resp => {
              console.log(resp);
              swal.fire(
                'User Deleted!',
                'You have Deleted this user',
                'success'
              )
              this.getAllUsers();


            },
            err => {
              console.log(err)
              swal.fire(
                'Something Bad Happend!',
                'Please try again',
                'error'
              )
            }

          )
        }
      })

  }

  addUser(Form) {
    console.log(Form)
    Form.clicked = true
    if (Form.valid) {
      this.user.image = 'https://ui-avatars.com/api/?background='+this.hexColor()+'&color=fff&bold=true&name='+this.user.nom+' '+this.user.prenom 

        this.usersSer.addUser(this.user).subscribe(
          resp => {
            console.log(resp);
            swal.fire(

              resp['response'].toString(),
              '',
              'success'
            )
            document.getElementById('closeModal').click()
            this.getAllUsers();
          },
          err => {
            console.log(err)
            swal.fire(
              'Something Bad Happend!',
              'Please try again',
              'error'
            )
          }

        )
    }

  }
  getAllUsers() {
    this.usersSer.getAllUsers().subscribe(
      resp => {
        this.users = resp
      }
      , err => { console.log(err) }
    )
  }
  editUser(login) {

  }
  hexColor() {
    var max = 16777215
    return Math.floor(Math.random() * max).toString(16)
  }
}
