import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/services/user-service.service';
import { RouterLink, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public username: string = "";
  public password: string = "";
  error: string;
  syntaxerror:boolean =false;
  submited: boolean;
  constructor(public userService: UserServiceService, private router: Router,) { }

  ngOnInit() {
    console.log(window.location.pathname)
    if (localStorage.getItem('islogged')== 'true'){
      this.router.navigate(['/home'])
    }
  }

  loginSubmit(loginForm) {
    this.submited=true
    if (loginForm.valid) {
      this.syntaxerror=false
      this.userService.getUser(this.username, this.password).subscribe(
        resp => {
          localStorage.setItem("user",JSON.stringify(resp))
          localStorage.setItem("login",resp['username'])
          localStorage.setItem("islogged",'true')
          this.router.navigate(['/home'])
          console.log(resp)
        },
        err => {
          this.error = err.error
        }
      )
    }
    else {
      
      this.syntaxerror=true
    }

  }
}