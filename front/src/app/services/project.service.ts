import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from '../models/Porject';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {



  mainUrl="http://127.0.0.1:8081/"
  urlAllProjects = this.mainUrl+'projects/getAll'
  urlremoveProject = this.mainUrl+'projects/Delet/'
  url_manage_Project=this.mainUrl+'projects'
  constructor(public http: HttpClient) {
    
  }


  getAllProjects() : Observable<Project[]>  {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    return this.http.get<Project[]>(this.urlAllProjects, { headers: headers })

  }

  removeProject(nom): Observable<any>  {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    return this.http.delete<any>(this.urlremoveProject+"/"+nom, { headers: headers })

  }
  addProject(project): Observable<Project> {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    if (project.id){

      var url=this.url_manage_Project+"/updateProject"
    }else{
      var url=this.url_manage_Project+"/addProject"
    }
    return this.http.post<Project>(url, project, { headers: headers })
  }
}
