import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  mainUrl="http://127.0.0.1:8081/"
  url = this.mainUrl+'users/login'
  urlAllUsers = this.mainUrl+'users/getAll'
  urlremoveUser = this.mainUrl+'users/DeletByEmail'
  url_manage_user=this.mainUrl+'users'

  constructor(public http: HttpClient) {
    
  }

  getUser(username, password): Observable<User> {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });

    return this.http.post<User>(this.url, { login: username, password: password }, { headers: headers })
  }
  getAllUsers() : Observable<User[]>  {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    return this.http.get<User[]>(this.urlAllUsers, { headers: headers })
  }

  removeUser(login): Observable<any>  {
    let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    return this.http.delete<any>(this.urlremoveUser+"/"+login, { headers: headers })

  }
  addUser(user): Observable<User> {
    try {
      let headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
      if (user.id){
        var url=this.url_manage_user+"/updateUser"
      }else{
        var url=this.url_manage_user+"/addUser"
  
      }
      return this.http.post<User>(url, user, { headers: headers })
    
    } catch (error) {
      console.log(error)
    }
  }
}
